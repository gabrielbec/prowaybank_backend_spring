package br.com.prowaybank.app.config;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import br.com.prowaybank.app.model.Aplicacao;
import br.com.prowaybank.app.model.Previdencia;
import br.com.prowaybank.app.service.AplicacaoService;
import br.com.prowaybank.app.service.PrevidenciaService;

@Configuration
public class addPrevidencias implements CommandLineRunner{

	@Autowired
	private PrevidenciaService previdenciaService;
	
	@Autowired
	private AplicacaoService aplicacaoService;

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		Aplicacao aplicacao = new Aplicacao(LocalDate.of(2020, 3, 3), 350.00);
		aplicacao = aplicacaoService.insert(aplicacao);
		
		Aplicacao aplicacao2 = new Aplicacao(LocalDate.of(2019, 3, 3), 550.00);
		aplicacao2 = aplicacaoService.insert(aplicacao2);
		
		List<Aplicacao> investimentos = Arrays.asList(aplicacao, aplicacao2);
		
		Previdencia previdencia = new Previdencia(0, 0, 0, 0, 0, 0.05, LocalDate.now(), investimentos);
		
		previdenciaService.insert(previdencia);
	}
	
}
