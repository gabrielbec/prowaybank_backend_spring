package br.com.prowaybank.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.prowaybank.app.model.InvestimentoExterno;

public interface InvestimentoExternoRepository extends JpaRepository<InvestimentoExterno, Long> {

}
