package br.com.prowaybank.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.prowaybank.app.model.Previdencia;

@Repository
public interface PrevidenciaRepository extends JpaRepository<Previdencia, Long> {

}
