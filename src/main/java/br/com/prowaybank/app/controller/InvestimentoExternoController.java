package br.com.prowaybank.app.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.com.prowaybank.app.model.InvestimentoExterno;
import br.com.prowaybank.app.repository.InvestimentoExternoRepository;

@RestController
public class InvestimentoExternoController {
	
	@Autowired
	private InvestimentoExternoRepository invExtRepository;
	
	
	@GetMapping("/investimento")
	public List<InvestimentoExterno> getAll(){
		List<InvestimentoExterno> investimentoExterno = invExtRepository.findAll();
		return investimentoExterno;
	}
	
	@GetMapping("/investimento/valorTotal")
	public double rendimentoTotal() {
		double valorTotal = 0;
		
		for( InvestimentoExterno n : invExtRepository.findAll() ) {
			valorTotal += ( n.getValorInvestido() * n.getVariacao() ) / 100;
		}
		
		return valorTotal;
	}
	
	@GetMapping("/investimento/rendimentoParticular")
	public ArrayList<InvestimentoExterno> rendimentoParticular(){
		ArrayList<InvestimentoExterno> rendimentoMesal = new ArrayList<InvestimentoExterno>();
		
		for ( InvestimentoExterno n : invExtRepository.findAll() ) {
			
			n.setRendimento(( n.getValorInvestido() * n.getVariacao() ) / 100);
			
			rendimentoMesal.add(n);
			 
			}																
		
		rendimentoMesal.sort((o1, o2) -> o1.getDataInvestimento().compareTo(o2.getDataInvestimento()));
		
		return rendimentoMesal;
		
	}
	
	@GetMapping("/investimento/rendimentoMesEspecifico/{mes}")
	public List<InvestimentoExterno> rendimentoMesEspecifico(@PathVariable("mes") int mes){
		List<InvestimentoExterno> investimentoMesEspecifico = new ArrayList<InvestimentoExterno>();
		
		investimentoMesEspecifico = rendimentoParticular().stream().filter(n -> n.getDataInvestimento().getMonthValue() == mes)
				.collect(Collectors.toList());
		
		return investimentoMesEspecifico;
	}
}
