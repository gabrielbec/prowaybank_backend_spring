package br.com.prowaybank.app.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.prowaybank.app.model.Aporte;
import br.com.prowaybank.app.model.Usuario;

public class AporteController {

	public Aporte aporte;

	public AporteController(Aporte aporte) {
		this.aporte = aporte;
	}

	/**
	 * Validar nome completo do usu�rio.
	 * 
	 * @param nome tem que ter menos que determinada quantidade de letras.
	 * @return
	 */
	final public Boolean verificaNomeCompleto() {
		if (this.aporte.getUsuario().getNomeCompleto().length() < 5) {
			return true;
		}
		return false;
	}

	/**
	 * Validar nome completo do usu�rio.
	 * 
	 * @param nome n�o pode ficar vazio.
	 * @return
	 */
	final public Boolean nomeCompletoVazio() {
		if (this.aporte.getUsuario().getNomeCompleto().isBlank()) {
			return false;
		}
		return true;
	}

	/**
	 * Validar nome do benefici�rio.
	 * 
	 * @param nome tem que ter menos que determinada quantidade de letras.
	 * @return
	 */
	final public Boolean verificarNomeBeneficiario() {
		if (this.aporte.getNomeBeneficiario().length() < 5) {
			return true;
		}
		return false;
	}

	/**
	 * Validar nome do benefici�rio.
	 * 
	 * @param nome n�o pode ficar vazio.
	 * @return
	 */
	final public Boolean nomeBeneficiarioVazio() {
		if (this.aporte.getNomeBeneficiario().isBlank()) {
			return false;
		}
		return true;
	}

	/**
	 * Validar valor em outro plano de previd�ncia.
	 * 
	 * @param valor n�o pode estar vazio.
	 * @return
	 */
	final public Boolean outraPrevidencia() {
		if (this.aporte.getValorOutro() == null) {
			return false;
		}
		return true;
	}

	/**
	 * Validar renda mensal que ser� recebida.
	 * 
	 * @param valor n�o pode estar vazio.
	 * @return
	 */
	final public Boolean rendaMensal() {
		if (this.aporte.getRendaMensal() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * Valor mensal que ser� aplicado.
	 * 
	 * @param valor n�o pode estar vazio.
	 * @return
	 */
	final public Boolean valorMensal() {
		if (this.aporte.getValorAplicado() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * Validar data de nascimento.
	 * 
	 * @param data n�o aceita datas falsas.
	 * @return
	 */
	final public Boolean data() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setLenient(false);
			sdf.parse(this.aporte.getUsuario().getDataNascimento());
			return true;
		} catch (ParseException ex) {
			return false;
		}
	}

	/**
	 * Validar se a data � maior que hoje.
	 * 
	 * @param data
	 * @return
	 */
	final public Boolean dataMenorQueHoje() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate dataVerificada = LocalDate.parse(this.aporte.getUsuario().getDataNascimento(), dtf);
		LocalDate hoje = LocalDate.now();
		if (dataVerificada.compareTo(hoje) <= 0) {
			return true;
		}
		return false;
	}

	/**
	 * Validar o sexo do usu�rio.
	 * 
	 * @param sexo n�o pode ficar vazio.
	 * @return
	 */
	final public Boolean sexoVazio() {
		if (this.aporte.getUsuario().getSexo().isBlank()) {
			return false;
		}
		return true;
	}

	/**
	 * Validar resgate �nico.
	 * 
	 * @param valor n�o pode estar vazio.
	 * @return
	 */
	final public Boolean resgateUnico() {
		if (this.aporte.getResgateUnico() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * Verifica se todos os atributos de aporte est�o corretos
	 * 
	 * @param aporte
	 * @return
	 */
	final public Boolean verificaDadosAporte() {
		boolean verificaNomeCompleto = verificaNomeCompleto() && nomeCompletoVazio();
		boolean verificaNomeBeneficiario = verificarNomeBeneficiario() && nomeBeneficiarioVazio();
		boolean verificaOutraPrevidencia = outraPrevidencia();
		boolean verificaRendaMensal = rendaMensal();
		boolean verificaValorMensal = valorMensal();
		boolean verificaData = data() && dataMenorQueHoje();
		boolean verificaSexo = sexoVazio();
		boolean verificaResgateUnico = resgateUnico();

		if (verificaNomeCompleto && verificaNomeBeneficiario && verificaOutraPrevidencia && verificaRendaMensal
				&& verificaValorMensal && verificaData && verificaSexo && verificaResgateUnico) {
			return true;
		}

		return false;
	}

	/**
	 * M�todo utilizado para guardar o aporte
	 * 
	 * @param aporte  Dados que ser�o salvos
	 * @param aportes lista onde ser� salvo o aporte
	 * @return retorna o aporte que foi salvo
	 * @throws Exception caso algum dos dados do aporte esteja incorreto, uma
	 *                   excess�o sera chamada
	 */
	final public Aporte salvarDados(Aporte aporte, List<Aporte> aportes) throws Exception {
		if (!verificaDadosAporte()) {
			throw new Exception("Dados incorretos");
		}

		aportes.add(aporte);
		System.out.println("Aporte de id " + (aportes.indexOf(aporte) + 1) + " adicionado");
		return aporte;
	}

	/**
	 * busca todos os aportes registrados
	 * 
	 * @return retorna uma lista de aportes
	 */
	final public List<Aporte> buscarTodosAportes() {
		List<Aporte> aportes = new ArrayList<Aporte>();
		aportes.add(new Aporte("Yuri", "350", 10, 10, 500, false, false, new Usuario("Ana", "12/02/2003", "Feminino")));
		aportes.add(
				new Aporte("Yuri", "450", 10, 10, 510, true, true, new Usuario("Lucio", "12/02/2004", "Masculino")));

		return aportes;
	}

	/**
	 * M�todo para buscar lista de aportes de acordo com o beneficiario
	 * 
	 * @param beneficario
	 * @param aportes
	 * @return retrona uma lista de aportes
	 */
	final public List<Aporte> buscarPorBeneficiario(String beneficario, List<Aporte> aportes) {
		List<Aporte> aportesSelecionados = aportes.stream().filter(ap -> ap.getNomeBeneficiario().equals(beneficario))
				.collect(Collectors.toList());
		return aportesSelecionados;
	}

}
