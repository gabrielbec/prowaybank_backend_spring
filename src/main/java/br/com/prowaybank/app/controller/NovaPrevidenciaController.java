package br.com.prowaybank.app.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.prowaybank.app.model.Aplicacao;
import br.com.prowaybank.app.model.Extrato;
import br.com.prowaybank.app.model.Previdencia;
import br.com.prowaybank.app.model.dao.PrevidenciaDAO;
import br.com.prowaybank.app.service.AplicacaoService;
import br.com.prowaybank.app.service.PrevidenciaService;

@RestController
@RequestMapping("previdencias")
public class NovaPrevidenciaController {

	PrevidenciaDAO prevDao = new PrevidenciaDAO();
	
	@Autowired
	private PrevidenciaService previdenciaService;
	
	@Autowired
	private AplicacaoService aplicacaoService;

	@GetMapping
	public List<Previdencia> listarPrevidencias() {
		return previdenciaService.findAll();
	}

	/**
	 * Retorna o extrato com a lista de aportes e o saldo atual com rendimento
	 * 
	 * @return
	 */
	@GetMapping("/extrato")
	public Extrato extrato() {
		Extrato extrato = new Extrato(previdenciaService.getById((long) 1.0));

		return extrato;
	}

	/**
	 * Adiciona aplica��o na lista de investimentos
	 * 
	 * @param valor
	 */
	@PostMapping("/add/aplicacao")
	public Aplicacao fazerAplicacao(@RequestBody Aplicacao app) {
		app.setData(LocalDate.now());
		app.setPrevidencia(previdenciaService.getById((long) 1.0));
		aplicacaoService.insert(app);
		
		Previdencia prev = previdenciaService.getById((long) 1.0);
		prev.adicionarInvestimento(app);
		previdenciaService.insert(prev);
		
		return app;
	}

	/**
	 * Adiciona Previdencia ao banco de dados
	 * 
	 * @param prev
	 * @return
	 */
	@PostMapping("/add/previdencia")
	public Previdencia criarPrevidencia(@RequestBody Previdencia prev) {
		//this.prevDao.criarPrevidencia(prev);
		return previdenciaService.insert(prev);
	}
}
