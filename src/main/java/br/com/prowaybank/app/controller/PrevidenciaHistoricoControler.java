package br.com.prowaybank.app.controller;

import br.com.prowaybank.app.model.Previdencia;

public class PrevidenciaHistoricoControler {
	public Previdencia prev;
	
	public PrevidenciaHistoricoControler(Previdencia prev) {
		this.prev = prev;
	}


	/**
	 * Metodo que calcula a quantidade de anos investido, e o valor investido ao
	 * longo desses anos, sem juros
	 * 
	 * @return prev.getValorInvestidoAnos()
	 */
	public double calcularAnosInvestimentoSemJuros() {	
		//int idadeA = prev.getIdadeAposentadoria();
		
		this.prev.setQuantidadeAnosInvestido(this.prev.getIdadeAposentadoria() - this.prev.getIdadeIncial()); 
		
		double quantidadeMesesInvestindo = this.prev.getQuantidadeAnosInvestido() * 12;
		
		this.prev.setValorInvestidoAnos(this.prev.getInvestimentoMensal() * quantidadeMesesInvestindo);

		return this.prev.getValorInvestidoAnos();
	}
	
	
	
	/**
	 * Metodo que retorna um array de investimentos
	 * 
	 * @return array com o investimento ao longo dos anos
	 */
	public double[] verificarTotalInvestimentoAnosComJuros() {
		double investimentoAnual =  this.prev.getInvestimentoMensal() * 12; //12.000
		
		double investidoAnoComJuros = investimentoAnual;
		
		this.prev.setValorInvestidoAnos(investimentoAnual);
		
		double[] vetorInvestimentoAnosComJuros = new double[(int) this.prev.getValorInvestidoAnos()];

		
		for(int i=0; i < this.prev.getQuantidadeAnosInvestido(); i++ ) {
			investidoAnoComJuros += investimentoAnual * this.prev.getJUROS_ANO() /12;
			
			vetorInvestimentoAnosComJuros[i] = investidoAnoComJuros;
		}
		
			
		return vetorInvestimentoAnosComJuros;
		
		//return this.desmembrarArrayInvestimento(vetorInvestimentoAnosComJuros);
		
	}
	
	/**
	 * Metodo para desmembrar cada posi��o do array, e mostrar cada ano individualmente no gr�fico
	 *  ### Ainda em implementa��o ###
	 * @param vetorInvestimentoAnosComJuros
	 * @return
	 */
	public double desmembrarArrayInvestimento(double[] vetorInvestimentoAnosComJuros) {
		double x = 0 ;
		
		for(int i=0; i < vetorInvestimentoAnosComJuros.length; i++) {
			x = vetorInvestimentoAnosComJuros[i];
		}
		return x;
	}
}
