package br.com.prowaybank.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.prowaybank.app.model.Previdencia;
import br.com.prowaybank.app.repository.PrevidenciaRepository;

@Service
public class PrevidenciaService {

	@Autowired
	private PrevidenciaRepository previdenciaRepository;
	
	public List<Previdencia> findAll(){
		return previdenciaRepository.findAll();
	}
	
	public Previdencia insert(Previdencia previdencia) {
		return previdenciaRepository.save(previdencia);
	}
	
	public Previdencia getById(Long id) {
		return previdenciaRepository.getById(id);
	}
	
}
