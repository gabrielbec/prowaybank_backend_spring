package br.com.prowaybank.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.prowaybank.app.model.Aplicacao;
import br.com.prowaybank.app.repository.AplicacaoRepository;

@Service
public class AplicacaoService {

	@Autowired
	private AplicacaoRepository aplicacaoRepository;
	
	public Aplicacao insert(Aplicacao aplicacao) {
		return aplicacaoRepository.save(aplicacao);
	}
	
}
