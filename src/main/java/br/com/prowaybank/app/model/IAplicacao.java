package br.com.prowaybank.app.model;

import java.time.LocalDate;

public interface IAplicacao {

	LocalDate getData();
	double getValor();
	
}
