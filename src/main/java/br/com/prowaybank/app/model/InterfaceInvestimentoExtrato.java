package br.com.prowaybank.app.model;

import java.util.List;

public interface InterfaceInvestimentoExtrato {

	List<IAplicacao> getInvestimentos();
	double getTaxaDeJuros();
	
}
