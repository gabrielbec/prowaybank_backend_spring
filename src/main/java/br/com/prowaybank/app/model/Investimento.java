package br.com.prowaybank.app.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class Investimento implements InvestimentoInterface, InterfaceInvestimentoExtrato {

	private double valorInvestido;
	private int tempoInvestimento;
	private double valorEstimado;
	private float taxaInvestimento;
	private LocalDate dataInvestimento;
	private double valorMinimoInvestimento;
	private List<Aplicacao> aplicacoes;

	/**
	 * Getters e Setters da classe Investimento
	 * 
	 * @return
	 */
	public double getValorInvestido() {
		return valorInvestido;
	}

	public List<Aplicacao> getAplicacoes() {
		return aplicacoes;
	}

	public void setAplicacoes(List<Aplicacao> aplicacoes) {
		this.aplicacoes = aplicacoes;
	}

	public void setValorInvestido(double valorInvestido) {
		this.valorInvestido = valorInvestido;
	}

	public int getTempoInvestimento() {
		return tempoInvestimento;
	}

	public void setTempoInvestimento(int tempoInvestimento) {
		this.tempoInvestimento = tempoInvestimento;
	}

	public double getValorEstimado() {
		return valorEstimado;
	}

	public void setValorEstimado(double valorEstimado) {
		this.valorEstimado = valorEstimado;
	}

	public float getTaxaInvestimento() {
		return taxaInvestimento;
	}

	public void setTaxaInvestimento(float taxaInvestimento) {
		this.taxaInvestimento = taxaInvestimento;
	}

	public LocalDate getDataInvestimento() {
		return dataInvestimento;
	}

	public void setDataInvestimento(LocalDate dataInvestimento) {
		this.dataInvestimento = dataInvestimento;
	}

	public double getValorMinimoInvestimento() {
		return valorMinimoInvestimento;
	}

	public void setValorMinimoInvestimento(double valorMinimoInvestimento) {
		this.valorMinimoInvestimento = valorMinimoInvestimento;
	}

	public String[] getInvestimentoGeral() {
		return new String[] { "lista com todos os investimentos" };
	}

	@Override
	public void setValorInvestido() {
		// TODO Auto-generated method stub

	}

	@Override
	public List<IAplicacao> getInvestimentos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double getTaxaDeJuros() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setDataInvestimento(Date dataInvestimento) {
		// TODO Auto-generated method stub
		
	}

}
