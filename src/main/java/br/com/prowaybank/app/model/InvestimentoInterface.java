package br.com.prowaybank.app.model;

import java.time.LocalDate;
import java.util.Date;

public interface InvestimentoInterface {

	double getValorInvestido();

	void setValorInvestido();

	int getTempoInvestimento();

	void setTempoInvestimento(int tempoInvestimento);

	double getValorEstimado();

	void setValorEstimado(double valorEstimado);

	float getTaxaInvestimento();

	void setTaxaInvestimento(float taxaInvestimento);

	LocalDate getDataInvestimento();

	void setDataInvestimento(Date dataInvestimento);

	double getValorMinimoInvestimento();

	void setValorMinimoInvestimento(double valorMinimoInvestimento);

	public String[] getInvestimentoGeral();
}
