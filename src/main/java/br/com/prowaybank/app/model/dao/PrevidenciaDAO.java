package br.com.prowaybank.app.model.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.prowaybank.app.model.Aplicacao;
import br.com.prowaybank.app.model.IAplicacao;
import br.com.prowaybank.app.model.Previdencia;

public class PrevidenciaDAO {

	public List<Previdencia> prevs = new ArrayList<Previdencia>();

	public PrevidenciaDAO() {
		List<IAplicacao> investimentos = 
				Arrays.asList(new Aplicacao(LocalDate.of(2020, 3, 3), 350.00), new Aplicacao(LocalDate.of(2019, 3, 3), 550.00));
		//this.prevs.add(new Previdencia(0, 0, 0, 0, 0, 0.05, LocalDate.now(), investimentos));
		//this.prevs.add(new Previdencia(0, 0, 0, 0, 0, 0.06, LocalDate.now(), investimentos));
	}
	
	public List<Previdencia> getAll(){
		return this.prevs;
	}
	
	public void criarPrevidencia(Previdencia prev) {
		this.prevs.add(prev);
	}
	
	public void adicionarAplicacao(int indexDaPrev, Aplicacao app) {
		prevs.get(indexDaPrev).adicionarInvestimento(app);
	}
}
