package br.com.prowaybank.app.model;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

public class Extrato {
	
	private List<Aplicacao> investimentos;
	private Double taxaDeJuros;
	
	public Extrato() {
		super();
	}

	public Extrato(Previdencia prev) {
		super();
		this.investimentos = prev.getInvestimentos();
		this.taxaDeJuros = prev.getTaxaDeJuros();
	}

	public List<Aplicacao> getInvestimentos() {
		return investimentos;
	}
	
	public Double getSaldo() {
		return this.getSaldoComRentabilidade();
	}

	/**
	 * Retorna o saldo com rentabilidade
	 * 
	 * @return
	 */
	private double getSaldoComRentabilidade() {
		double saldo = 0;
		//double resultado = this.calculoRendimento(saldo, dataInicial);		
		for(IAplicacao ip : this.investimentos) {
			saldo += this.calculoRendimento(ip.getValor(), ip.getData());
		}
		
		return saldo;
	}
	
	/**
	 * Calcula o rendimento de acordo com o valor investido
	 * 
	 * @param valorInvestido
	 * @param data
	 * @return
	 */
	private double calculoRendimento(double valorInvestido, LocalDate data) {
		return valorInvestido * Math.pow((1 + this.taxaDeJuros), this.diferencaEntreMeses(data));
	}

	/**
	 * Retorna a diferen�a em meses entre a data atual e data passada por paramentro
	 * 
	 * @param data
	 * @return
	 */
	private int diferencaEntreMeses(LocalDate data) {
		LocalDate dataAtual = LocalDate.now();
		Period comparar = Period.between(data, dataAtual);
		return comparar.getMonths();
	}

}
