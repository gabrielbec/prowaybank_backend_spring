package br.com.prowaybank.app.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class InvestimentoExterno {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String empresa;
	private String setor; 
	private double valorInvestido;
	private double variacao;
	private LocalDateTime dataInvestimento;
	private double rendimento;
    
	public double getRendimento() {
		return rendimento;
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public void setRendimento(double rendimento) {
		this.rendimento = rendimento;
	}

	public String getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getSetor() {
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	public double getValorInvestido() {
		return valorInvestido;
	}
	public void setValorInvestido(double valorInvestido) {
		this.valorInvestido = valorInvestido;
	}
	public double getVariacao() {
		return variacao;
	}
	public void setVariacao(double variacao) {
		this.variacao = variacao;
	}
	public LocalDateTime getDataInvestimento() {
		return dataInvestimento;
	}
	public void setDataInvestimento(LocalDateTime dataInvestimento) {
		this.dataInvestimento = dataInvestimento;
	}

}

