package br.com.prowaybank.app.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Aplicacao implements IAplicacao{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private LocalDate data;
	private Double valor;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "previdencia_id")
	private Previdencia previdencia;

	public Aplicacao() {
		super();
	}

	public Aplicacao(LocalDate data, Double valor) {
		super();
		this.data = data;
		this.valor = valor;
	}

	public Aplicacao(Long id, LocalDate data, Double valor, Previdencia previdencia) {
		super();
		this.id = id;
		this.data = data;
		this.valor = valor;
		this.previdencia = previdencia;
	}

	public Previdencia getPrevidencia() {
		return previdencia;
	}

	public void setPrevidencia(Previdencia previdencia) {
		this.previdencia = previdencia;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public LocalDate getData() {
		// TODO Auto-generated method stub
		return this.data;
	}

	@Override
	public double getValor() {
		// TODO Auto-generated method stub
		return this.valor;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}
